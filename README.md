# partis example usage

My from-scratch [partis] setup via conda, plus some notes on basic usage as I
worked through things.

## Setup

Setting up conda environment with dependencies:

    conda env update --file environment.yml
    conda activate example-partis

(Pro tip: use [mamba](https://mamba.readthedocs.io/en/latest/index.html) as a
drop-in replacement for conda for an enormous speed-up of dependency
resolution.)

Setting up partis itself:

    # Lots of additional software is supplied via git submodules, so we'll make
    # sure those are all available
    git submodule update --init --recursive
    ( cd partis && ./bin/build.sh with-simulation )
    # Dependencies are installed in $CONDA_PREFIX but partis itself lives right
    # here, so we'll put it on the PATH
    export PATH="$PATH:$PWD/partis/bin"

Compiling all the different components via the build script can take a while.

The PATH change could be [incorporated into the conda environment](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html),
too, instead of being manual.  Also, the partis repo is huge (> 1 GB with all
submodules included) so I'm keeping a single copy separate from the conda
environments (though in this example it's contained right here as a
subdirectory).

## Usage

### Running the Tests

Running all tests (paired/non-paired, slow/non-slow) with unlimited print width
so we can see everything, and from inside the partis repository since the test
script expects that:

    $ ( cd partis && ./test/test.py --run-all --print-width 0) 2>&1 | tee partis_tests.log
    run ./test/test.py --print-width 0
    cache-parameters-simu            .../example-partis/partis/bin/partis cache-parameters --dont-write-git-info --infname test/ref-results/test/simu.yaml --parameter-dir test/new-results/test/parameters/simu --sw-cachefname test/new-results/test/parameters/simu/sw-cache.yaml --is-simu --random-seed 1 --n-procs 10
    annotate-new-simu                .../example-partis/partis/bin/partis annotate --dont-write-git-info --infname test/ref-results/test/simu.yaml --parameter-dir test/new-results/test/parameters/simu --sw-cachefname test/new-results/test/parameters/simu/sw-cache.yaml --plot-annotation-performance --is-simu --plotdir test/new-results/annotate-new-simu-annotation-performance --only-csv-plots --random-seed 1 --n-procs 10 --outfname test/new-results/annotate-new-simu.yaml
    ...

It complains a little for me about changes in runtimes and counts of output
files, but everything seems to run OK in the end.  I wasn't totally clear from
the docs how to supply paired heavy and light chain sequences, but the test
script includes an example that scrolls by as it runs:

    run ./test/test.py --print-width 0 --paired
    cache-parameters-simu            .../example-partis/partis/bin/partis cache-parameters --dont-write-git-info --paired-loci --paired-indir test/paired/ref-results/test/simu --parameter-dir test/paired/new-results/test/parameters/simu --is-simu --random-seed 1 --n-procs 10
    partition-new-simu               .../example-partis/partis/bin/partis partition --dont-write-git-info --paired-loci --paired-indir test/paired/ref-results/test/simu --parameter-dir test/paired/new-results/test/parameters/simu --plot-annotation-performance --max-ccf-fail-frac 0.10 --is-simu --plotdir test/paired/new-results/partition-new-simu-annotation-performance --only-csv-plots --no-partition-plots --random-seed 1 --n-procs 10 --paired-outdir test/paired/new-results/partition-new-simu
        chose seed uids 3542bd38d8-igh igk from heavy cluster with size 10 (was asked for size in [5, 10]) (chose from among 10 heavy clusters with sizes 12 10 9 7 6 3 3 2 2 2)
    seed-partition-new-simu          .../example-partis/partis/bin/partis partition --dont-write-git-info --paired-loci --paired-indir test/paired/ref-results/test/simu --parameter-dir test/paired/new-results/test/parameters/simu --max-ccf-fail-frac 0.10 --is-simu --seed-unique-id 3542bd38d8-igh:3542bd38d8-igk --seed-loci igh:igk --random-seed 1 --n-procs 10 --paired-outdir test/paired/new-results/seed-partition-new-simu
    get-selection-metrics-new-simu   .../example-partis/partis/bin/partis get-selection-metrics --dont-write-git-info --paired-loci --existing-output-run-cfg paired --min-selection-metric-cluster-size 3 --min-paired-cluster-size-to-read 3 --random-seed 1 --n-procs 10 --paired-outdir test/paired/new-results/partition-new-simu --chosen-ab-fname test/paired/new-results/get-selection-metrics-new-simu-chosen-abs.csv
    cache-parameters-data            .../example-partis/partis/bin/partis cache-parameters --dont-write-git-info --paired-loci --infname test/paired-data/all-seqs.fa --parameter-dir test/paired/new-results/test/parameters/data --n-max-queries 100 --random-seed 1 --n-procs 10
    simulate                         .../example-partis/partis/bin/partis simulate --dont-write-git-info --paired-loci --parameter-dir test/paired/new-results/test/parameters/data --n-sim-events 10 --n-trees 10 --n-leaf-distribution geometric --n-leaves 5 --min-observations-per-gene 5 --mean-cells-per-droplet 1.25 --constant-cells-per-droplet --fraction-of-reads-to-remove 0.15 --random-seed 1 --n-procs 10 --paired-outdir test/paired/new-results/test/simu --indel-frequency 0.2

(You can view the output again in all its colorful glory with
`less -SR partis_tests.log`.)

Some relevant options from `partis partition --help`:

      --paired-loci         Set this if input contains sequences from more than
                            one locus (igh+igk+igl all together). Input can be
                            specified either with --infname (in which case it
                            will be automatically split apart by loci), or with
                            --paired-indir (whose files must conform to the
                            same conventions). It will then run the specified
                            action on each of the single locus input files, and
                            (if specified) merge the resulting partitions.
                            (default: False)
      --infname INFNAME     input sequence file in .fa, .fq, .csv, or partis
                            output .yaml (if .csv, specify id string and
                            sequence headers with --name-column and
                            --seq-column) (default: None)
      --paired-indir PAIRED_INDIR
                            Directory with input files for use with
                            --paired-loci.  Must conform to file naming
                            conventions from bin/split-loci.py (really
                            paircluster.paired_dir_fnames()), i.e. the files
                            generated when --infname and --paired-loci are set.
                            (default: None)
      --input-metafnames INPUT_METAFNAMES
                            colon-separated list of yaml/json files, each of
                            which has meta information for the sequences in
                            --infname (and --queries-to-include-fname, although
                            that file can also include its own input meta
                            info), keyed by sequence id. If running multiple
                            steps (e.g.  cache-parameters and partition), this
                            must be set for all steps. See
                            https://github.com/psathyrella/partis/blob/master/docs/subcommands.md#input-meta-info
                            for an example. (default: None)

As hinted at by the help text for paired-indir, the input-metafnames option is
the way to supply pairing information when giving paired input in a FASTA via
`--infname` (the alternative being the already-split-and-prepared version
supplied via `--paired-indir`, where the metadata is supplied by YAML in that
directory).  In the tests, cache-parameters is explicitly run as a separate
step from those that follow with the already-prepared input directory
test/paired/ref-results/test/simu.

The docs example for `--input-metafnames` gives more general metadata examples,
but for paired sequences specifically, meta.yaml in the simu directory above
shows the way.  From `partis/test/paired/ref-results/test/simu/meta.yaml`:

    {
       "09f5010bd3-igh" : {
          "locus" : "igh",
          "paired-uids" : [
             "09f5010bd3-igl",
             "cfe3b29587-igh",
             "cfe3b29587-igk"
          ]
       },
       "09f5010bd3-igl" : {
          "locus" : "igl",
          "paired-uids" : [
             "09f5010bd3-igh",
             "cfe3b29587-igh",
             "cfe3b29587-igk"
          ]
       },
       "0eb272ee70-igh" : {
          "locus" : "igh",
          "paired-uids" : [
             "0eb272ee70-igl"
          ]
       },
       "0eb272ee70-igl" : {
          "locus" : "igl",
          "paired-uids" : [
             "0eb272ee70-igh"
          ]
       },
    # ...


This way each sequence in a set references the other sequence (or more than
one, for cases more complex than the expected IGH+IGK or IGH+IGL).  For
example, `0eb272ee70-igh` has a single item in a list of `paired-uids`
pointing to `0eb272ee70-igl`, and vice versa, and each locus is defined
explicitly.  Those sequence IDs match what's in the all-seqs.fa file alongside
the YAML.

    $ grep 0eb272ee70 partis/test/paired/ref-results/test/simu/all-seqs.fa
    >0eb272ee70-igh
    >0eb272ee70-igl

### Trying it Out

So, if we just have an all-seqs.fa and a meta.yaml, we can create a full input
directory from those.  We should be able to use ones from the test directory
here.  The partis script has a few hardcoded instances of `./bin/...` so we
need to be inside the partis directory for this to work.

    $ cd partis
    $ partis cache-parameters --paired-loci \
        --infname test/paired/ref-results/test/simu/all-seqs.fa \
        --input-metafnames test/paired/ref-results/test/simu/meta.yaml \
        --parameter-dir ../paired-example-params \
        --paired-outdir ../paired-example-input
    .../partis/bin/partis:19: DeprecationWarning: distutils Version classes are deprecated. Use packaging.version instead.
      if StrictVersion(scipy.__version__) < StrictVersion('0.17.0'):
      note: set tree inference outdir to .../partis/../paired-example-input
      note: found 'paired-uids' in --input-metafnames, so not extracting pairing info
    run .../partis/bin/split-loci.py test/paired/ref-results/test/simu/all-seqs.fa --outdir ../paired-example-input
      running vsearch on 113 sequences:
    ...etc...
    $ cd ..

Now there's a paired input directory (paired-example-input) and a parameters
directory (paired-example-params):

    $ ls paired-example-*
    paired-example-input:
    igh.fa  igh+igk  igh+igl  igk.fa  igl.fa  meta.yaml
    
    paired-example-params:
    igh  igk  igl

We can run a partition command with that already-prepared input directory:

    $ partis partition --paired-loci --paired-indir paired-example-input --parameter-dir paired-example-params --paired-outdir paired-example-outdir
    .../partis/bin/partis:19: DeprecationWarning: distutils Version classes are deprecated. Use packaging.version instead.
      if StrictVersion(scipy.__version__) < StrictVersion('0.17.0'):
      note: set tree inference outdir to .../paired-example-outdir
    partition igh:
    run .../partis/bin/partis partition --locus igh --parameter-dir paired-example-params/igh --refuse-to-cache-parameters --infname paired-example-input/igh.fa --outfname paired-example-outdir/single-chain/partition-igh.yaml --sw-cachefname paired-example-params/igh/sw-cache.yaml
    .../partis/bin/partis:19: DeprecationWarning: distutils Version classes are deprecated. Use packaging.version instead.
      if StrictVersion(scipy.__version__) < StrictVersion('0.17.0'):
      note: set tree inference outdir to .../paired-example-outdir/single-chain/partition-igh
    partitioning     (with paired-example-params/igh/hmm)
    smith-waterman
      vsearch: 56 / 56 v annotations (0 failed) with 6 v genes in 0.1 sec
            reading sw results from paired-example-params/igh/sw-cache.yaml
          info for 56 / 56 = 1.000   (removed: 0 failed, 0 duplicates [removed when cache file was written])
          kept 25 (0.446) unproductive
            water time: 0.0
    hmm
      caching all 56 naive sequences (0.6s)
        collapsed 56 queries into 50 clusters with identical naive seqs (0.0 sec)
    ...

By the end the file `paired-example-outdir/partition-igh.yaml` doesn't just
have partition info for heavy chain (igh) sequences, but is also updated for
any info from light chain (igk/igl) sequences too. (See
[here](https://github.com/psathyrella/partis/blob/master/docs/paired-loci.md#output-directory)).

So what's the file layout?  See the [output formats
documentation](https://github.com/psathyrella/partis/blob/master/docs/output-formats.md).

There are command-line options for the number of partitions to include
(`--n-partitions-to-write`) and for which partitions the event details should
be written (`--write-additional-cluster-annotations`).  I think the description
of the annotation list (the events section) and the partition list (the
partition section) implies they are consistently ordered, such that the first
list of dictionaries of rearrangement events corresponds to the first
partition.  But, note the disclaimer about not assuming the first partition
listed is the most likely one.  (Open question: if the default number of
partitions is 10, why do I only see one here?)

In this case I have one event list (with one dictionary per recombination
event) corresponding to the one partition given.  To pick one example event,
the heavy chain and light chain sequence IDs (amongst other attributes) in the
first dictionary are:


     "paired-uids" : [
        ["35fc655801-igk"],
        ["7ba384a461-igk"]
     ],
     "unique_ids" : [
        "35fc655801-igh",
        "7ba384a461-igh"
     ],

This matches the first nested list within the one partition given lower in the file:


    "partitions" : [
       {
          "n_procs" : 1,
          "logprob" : 0,
          "n_clusters" : 21,
          "partition" : [
             [
                "35fc655801-igh",
                "7ba384a461-igh"
             ],


There are 21 total nested lists in that one partition with 21 total event
dictionaries to match.

What if we hadn't used paired information and just partitioned by heavy chain?
We can let it do the parameter setup again automatically instead of running
that in a separate command:

    $ partis partition --infname paired-example-input/igh.fa --outfname partition.yaml --write-full-yaml-output
      (DeprecationWarning etc etc)
      note: set tree inference outdir to .../partition
      note: --parameter-dir not set, so using default: _output/paired-example-input_igh
      parameter dir does not exist, so caching a new set of parameters before running action 'partition': _output/paired-example-input_igh
    caching parameters
    ...

(I've been reformatting the more limited JSON/YAML output with nice whitespace,
but we can get a more human-readable output from partis automatically with the
`--write-full-yaml-output` argument).

Now we get only the `-igh` IDs mentioned in the partition file, and the
partitioning isn't quite the same.  For one thing we do get the default 10
items (another open question: why now, but not before?) but still just the
annotation details for the one best-looking partition.  If we were to give
`--n-partitions-to-write 1` we would just get the one list of nested sequence
ID lists and the one set of annotations.  But in any case the best partitioning
here has 20 clusters instead of the 21 we have from the paired analysis.

    $ grep -E 'logprob|n_clusters' partition.yaml
    - logprob: -.inf
      n_clusters: 29
    - logprob: -.inf
      n_clusters: 28
    - logprob: -.inf
      n_clusters: 27
    - logprob: -.inf
      n_clusters: 26
    - logprob: -.inf
      n_clusters: 25
    - logprob: -5543.247469352582
      n_clusters: 24
    - logprob: -5514.6833233318175
      n_clusters: 23
    - logprob: -5484.951164317326
      n_clusters: 22
    - logprob: -5463.827659684829
      n_clusters: 21
    - logprob: -5445.32641362285
      n_clusters: 20

If I go back to the paired one and try to get more candidate partitions, I
still only get one in the output.  The log probability is also given as 0
(...100% probability?)?  Is this a consequence of some of the per-locus
detail being lost when merging information for the separate per-locus
partitionings together?

### Reproducibility

Re-running the same partition commands with the same inputs can give slightly
varying outputs.  What's going on there?  There's a configurable random seed
that just defaults to the current
([Unix](https://en.wikipedia.org/wiki/Unix_time)) time, which ensures random
behavior by default:

    $ partis partition --help | grep RANDOM_SEED$ -A 1; date +%s
      --random-seed RANDOM_SEED
                            Random seed used by many different things, but especially when reshuffling sequences between partitioning steps, and during simulation. Set this if you want to get exactly the same result when rerunning. (default: 1645635638)
    1645635638
    $ partis partition --help | grep RANDOM_SEED$ -A 1; date +%s
      --random-seed RANDOM_SEED
                            Random seed used by many different things, but especially when reshuffling sequences between partitioning steps, and during simulation. Set this if you want to get exactly the same result when rerunning. (default: 1645635646)
    1645635646

Setting that to a particular number makes the output fully consistent between
runs.

## Output Handling

Partis has a wonderfully information-rich command-line interface to the details
in these output files.

Summarizing the heavy-chain-only version:

    $ partis view-output --parameter-dir _output --outfname partition.yaml | less -SR
    partitions:
                   logprob   delta   index  clusters  n_procs     sizes
    
                  -inf                  0      29        4    6 6 5 4 4 3 2 2 2 2 2 (+18)                                  14195e4958-igh:e11acbe3ab-igh:...
                  -inf        nan       1      28        3    7 6 5 4 4 3 2 2 2 2 2 (+17)                                  8d79fca804-igh:b1e56e40a1-igh:...
                  -inf        nan       2      27        3    7 6 5 4 4 3 2 2 2 2 2 2 (+15)                                8d79fca804-igh:b1e56e40a1-igh:...
                  -inf        nan       3      26        2    7 6 6 5 4 3 2 2 2 2 2 (+15)                                  8d79fca804-igh:b1e56e40a1-igh:...
                  -inf        nan       4      25        2    7 6 6 5 4 3 2 2 2 2 2 2 (+13)                                8d79fca804-igh:b1e56e40a1-igh:...
                  -5543.25    inf       5      24        1    8 6 6 5 4 3 2 2 2 2 2 2 (+12)                                0eb272ee70-igh:8d79fca804-igh:...
                  -5514.68    28.6      6      23        1    8 7 6 5 4 3 2 2 2 2 2 2 (+11)                                0eb272ee70-igh:8d79fca804-igh:...
                  -5484.95    29.7      7      22        1    8 7 6 5 4 3 2 2 2 2 2 2 2 (+9)                               0eb272ee70-igh:8d79fca804-igh:...
              *   -5463.83    21.1      8      21        1    8 7 7 5 4 3 2 2 2 2 2 2 2 (+8)                               0eb272ee70-igh:8d79fca804-igh:...
          best    -5445.33    18.5      9      20        1    8 7 7 6 4 3 2 2 2 2 2 2 2 (+7)                               0eb272ee70-igh:8d79fca804-igh:...
    annotations:
    ...

So the best partition is the last one there, with the highest probability.
That's what the one set of annotations in the file refer to.  Lower down in the
output the sequence content with boundaries of V/D/J/nontemplated nucleotides
are shown, invariant codons highlighted, etc.

For the paired version:

    $ partis view-output --parameter-dir paired-example-params --outfname paired-example-outdir/partition-igh.yaml | less -SR

It shows the one partition identified, with the same sort of sequence
annotations given as for the heavy-chain-only version.

The parse-output script can convert a partition info file to other formats,
like FASTA:

    $ parse-output.py partition.yaml partition.fa
      found 20 clusters in best partition
        taking all 20 clusters
      writing 56 sequences to partition.fa

Or [AIRR TSV]:

    $ parse-output.py --airr-output partition.yaml partition.tsv
      found 20 clusters in best partition
        taking all 20 clusters
      writing 20 annotations (with partition: 56 seqs in 20 clusters) to partition.tsv
       writing airr annotations to partition.tsv

partis stores the parition information in the AIRR format's optional `clone_id`
column, grouping together sequences with the same rearrangement event under the
same clone ID.

[partis]: https://github.com/psathyrella/partis
[AIRR TSV]: https://docs.airr-community.org/en/stable/datarep/rearrangements.html
